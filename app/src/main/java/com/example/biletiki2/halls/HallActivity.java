package com.example.biletiki2.halls;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.biletiki2.R;
import com.example.biletiki2.halls.grosse_saal.GrosseSaal;
import com.example.biletiki2.halls.grosse_saal.areas.FirstArea;
import com.example.biletiki2.halls.grosse_saal.areas.SecondArea;
import com.example.biletiki2.halls.grosse_saal.areas.ThirdArea;
import com.example.biletiki2.halls.kleiner_saal.KleinerSaal;

public class HallActivity extends AppCompatActivity implements ChooseHall.HallClick, GrosseSaal.ChoosedArea {

    private FrameLayout fragContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hall);

        fragContainer = findViewById(R.id.frag_container);

        ChooseHall chooseHall = new ChooseHall();
        chooseHall.setListener(this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frag_container, chooseHall)
                .commit();
    }

    @Override
    public void onHallClick(int id) {
        if (id == R.id.grosse_saal_frame) {
            GrosseSaal grosseSaal = new GrosseSaal();
            grosseSaal.setListener(this);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frag_container,grosseSaal)
                    .addToBackStack(null)
                    .commit();
        } else if (id == R.id.kleiner_saal_frame) {

            Intent intent = new Intent(this,KleinerSaal.class);
            startActivity(intent);
        }
    }

    @Override
    public void onClickArea(int id) {
        if (id == R.id.first_area){
            Intent intent = new Intent(this, FirstArea.class);
            startActivity(intent);
        }else if (id == R.id.second_area){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frag_container,new SecondArea())
                    .addToBackStack(null)
                    .commit();
        }else if (id == R.id.third_area){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frag_container,new ThirdArea())
                    .addToBackStack(null)
                    .commit();
        }
    }
}
