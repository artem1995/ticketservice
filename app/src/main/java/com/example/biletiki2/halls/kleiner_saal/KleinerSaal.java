package com.example.biletiki2.halls.kleiner_saal;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;

import com.example.biletiki2.R;

public class KleinerSaal extends AppCompatActivity implements View.OnClickListener {
    private GridLayout row1, row2, row3, row4, row5, row6, row7, row8, row9;
    private FrameLayout l1, l2;
    private boolean color = false;
    private boolean color1 = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kleiner_saal);

        //rows
        row1 = findViewById(R.id.row1);
        row2 = findViewById(R.id.row2);
        row3 = findViewById(R.id.row3);
        row4 = findViewById(R.id.row4);
        row5 = findViewById(R.id.row5);
        row6 = findViewById(R.id.row6);
        row7 = findViewById(R.id.row7);
        row8 = findViewById(R.id.row8);
        row9 = findViewById(R.id.row9);

        l1 = findViewById(R.id.L1);
        l2 = findViewById(R.id.L2);
        l1.setOnClickListener(this);
        l2.setOnClickListener(this);

        row1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id){
            case R.id.L1:
                if(!color){
                    v.setBackgroundColor(Color.parseColor("#07ace8"));
                    color = true;
                }else if(color){
                    v.setBackgroundColor(Color.parseColor("#CC6E8F"));
                    color = false;
                }
                break;
            case R.id.L2:
                if(!color1){
                    v.setBackgroundColor(Color.parseColor("#07ace8"));
                    color1 = true;
                }else if(color1){
                    v.setBackgroundColor(Color.parseColor("#CC6E8F"));
                    color1 = false;
                }
                break;
        }
    }
}
