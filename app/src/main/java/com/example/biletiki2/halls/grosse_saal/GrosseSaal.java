package com.example.biletiki2.halls.grosse_saal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.biletiki2.R;

public class GrosseSaal extends Fragment implements View.OnClickListener {

    private ChoosedArea listener;
    private FrameLayout firstArea, secondArea, thirdArea;

    public void setListener(ChoosedArea listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grosse_saal, container, false);
        firstArea = view.findViewById(R.id.first_area);
        secondArea = view.findViewById(R.id.second_area);
        thirdArea = view.findViewById(R.id.third_area);
        firstArea.setOnClickListener(this);
        secondArea.setOnClickListener(this);
        thirdArea.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.first_area) {
            numOfArea(id);
        } else if (id == R.id.second_area) {
            numOfArea(id);
        } else if (id == R.id.third_area) {
            numOfArea(id);
        }
    }

    private void numOfArea(int id) {
        if (listener != null) {
            listener.onClickArea(id);
        }
    }

    public interface ChoosedArea {
        void onClickArea(int id);
    }
}
