package com.example.biletiki2.halls;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.biletiki2.R;

public class ChooseHall extends Fragment implements View.OnClickListener {

    private FrameLayout grosseSaal, kleinerSaal;
    private HallClick listener;

    public void setListener(HallClick listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.choose_hall, container, false);
        grosseSaal = view.findViewById(R.id.grosse_saal_frame);
        kleinerSaal = view.findViewById(R.id.kleiner_saal_frame);
        grosseSaal.setOnClickListener(this);
        kleinerSaal.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.grosse_saal_frame){
            hallId(id);
        }else if (id == R.id.kleiner_saal_frame){
            hallId(id);
        }
    }

    private void hallId(int id) {
        if (listener != null) {
            listener.onHallClick(id);
        }
    }

    interface HallClick{
        void onHallClick(int id);
    }
}
