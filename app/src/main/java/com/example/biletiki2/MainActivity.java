package com.example.biletiki2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.support.v7.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.biletiki2.halls.HallActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, SearchView.OnCloseListener, PopupMenu.OnMenuItemClickListener {

    private SearchView searchView;
    private TextView toolbarTxt, filtersTxt;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private LinearLayout calendarRow, eventTypeRow;
    private ImageView eventArrowDown, calendarArrowDown;
    private DatePicker datePicker;
    private LinearLayout popWindow;
    private FrameLayout underlineEvent, underlineCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorHamburger));
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//        toolbar.setNavigationIcon(null);

        searchView = findViewById(R.id.search_view);
        SearchView.SearchAutoComplete searchAutoComplete =
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(getResources().getColor(R.color.colorHamburger));
        searchAutoComplete.setTextColor(getResources().getColor(R.color.colorHamburger));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnSearchClickListener(this);
        searchView.setOnCloseListener(this);

        toolbarTxt = findViewById(R.id.toolbar_txt);

        datePicker = findViewById(R.id.date_picker);
        filtersTxt = findViewById(R.id.filters_txt);
        calendarRow = findViewById(R.id.calendar_row);
        eventTypeRow = findViewById(R.id.event_type_row);
        eventArrowDown = findViewById(R.id.event_arrow_down);
        calendarArrowDown = findViewById(R.id.calendar_arrow_down);
        popWindow = findViewById(R.id.popup_window);
        underlineEvent = findViewById(R.id.underline_event);
        underlineCalendar = findViewById(R.id.underline_calendar);
        eventTypeRow.setOnClickListener(this);
        calendarRow.setOnClickListener(this);
        eventArrowDown.setOnClickListener(this);
        calendarArrowDown.setOnClickListener(this);
        filtersTxt.setOnClickListener(this);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_login_out) {
            Intent intent = new Intent(this, LogoutActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_events) {

        } else if (id == R.id.nav_shopping_cart) {

        } else if (id == R.id.nav_halls_sheme) {
            Intent intent = new Intent(this, HallActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_terms_and_conditions) {
            Intent intent = new Intent(this, TermsAndConditions.class);
            startActivity(intent);
        } else if (id == R.id.nav_about_us) {

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.search_view) {
            if (eventTypeRow.getVisibility() == View.VISIBLE) {
                isFiltersVisible();
            }
            toolbarTxt.setVisibility(View.GONE);
        } else if (v.getId() == R.id.filters_txt) {
            if (datePicker.getVisibility() == View.VISIBLE) {
                showDate();
            }
            if (eventTypeRow.getVisibility() == View.VISIBLE) {
                isFiltersVisible();
            } else {
                eventTypeRow.setVisibility(View.VISIBLE);
                underlineEvent.setVisibility(View.VISIBLE);
                underlineCalendar.setVisibility(View.VISIBLE);
                calendarRow.setVisibility(View.VISIBLE);
            }
        } else if (v.getId() == R.id.calendar_arrow_down || v.getId() == R.id.calendar_row) {
            if (datePicker.getVisibility() == View.VISIBLE) {
                showDate();
                calendarArrowDown.setImageResource(R.drawable.ic_arrow_dropdown);
                datePicker.setVisibility(View.GONE);
            } else {
                calendarArrowDown.setImageResource(R.drawable.ic_arrow_dropdown_up);
                datePicker.setVisibility(View.VISIBLE);
            }

        } else if (v.getId() == R.id.event_arrow_down || v.getId() == R.id.event_type_row) {
            if (popWindow.getVisibility() == View.GONE) {
                popWindow.setVisibility(View.VISIBLE);
                eventArrowDown.setImageResource(R.drawable.ic_arrow_dropdown_up);
            } else {
                popWindow.setVisibility(View.GONE);
                eventArrowDown.setImageResource(R.drawable.ic_arrow_dropdown);
            }
        }
    }

    private void showDate() {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth() + 1;
        int year = datePicker.getYear();

        Toast.makeText(this, day + "/" + month + "/" + year, Toast.LENGTH_SHORT).show();
    }


    private void isFiltersVisible() {
        eventTypeRow.setVisibility(View.GONE);
        calendarRow.setVisibility(View.GONE);
        datePicker.setVisibility(View.GONE);
        popWindow.setVisibility(View.GONE);
        underlineEvent.setVisibility(View.GONE);
        underlineCalendar.setVisibility(View.GONE);
        calendarArrowDown.setImageResource(R.drawable.ic_arrow_dropdown);
        eventArrowDown.setImageResource(R.drawable.ic_arrow_dropdown);
    }

    @Override
    public boolean onClose() {
        toolbarTxt.setVisibility(View.VISIBLE);
        return false;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        item.setChecked(!item.isChecked());
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        item.setActionView(new View(this));
        item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return false;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return false;
            }
        });
        return false;
    }


}
