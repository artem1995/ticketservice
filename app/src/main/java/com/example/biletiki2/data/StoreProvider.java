package com.example.biletiki2.data;

import android.content.Context;

public class StoreProvider {
    private static final StoreProvider instance = new StoreProvider();
    private static final String SP_AUTH = "AUTH";
    private static final String SP_DATA = "DATA";
    private static final String CURR = "CURR";
    private static final String TOKEN = "TOKEN";
    private Context context;

    private StoreProvider() {
    }

    public static StoreProvider getInstance() {
        return instance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void save(String token){
        context.getSharedPreferences(SP_AUTH, Context.MODE_PRIVATE)
                .edit()
                .putString(TOKEN, token)
                .commit();
    }

    public String getToken(){
        return context.getSharedPreferences(SP_AUTH,Context.MODE_PRIVATE)
                .getString(TOKEN,null);
    }

    public boolean login(String email, String password) {
        return context.getSharedPreferences(SP_AUTH, Context.MODE_PRIVATE)
                .edit()
                .putString(CURR, email + "&" + password)
                .commit();
    }

    public boolean logout() {
        return context.getSharedPreferences(SP_AUTH, Context.MODE_PRIVATE)
                .edit()
                .remove(TOKEN)
                .commit();
    }
}

