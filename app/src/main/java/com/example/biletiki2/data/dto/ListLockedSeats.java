package com.example.biletiki2.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ListLockedSeats {
    private List<LockedSeats> list;

    public ListLockedSeats() {
        this.list = new ArrayList<>();
    }

    public void add(LockedSeats seats){
        list.add(seats);
    }

    public List<LockedSeats> getList() {
        return list;
    }
}
