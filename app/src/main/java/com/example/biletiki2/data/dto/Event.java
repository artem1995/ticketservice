package com.example.biletiki2.data.dto;

public class Event {
    private long eventId, eventStart, hall, ticketsAvailable, minPrice, maxPrice;
    private String eventName, artist, description, eventType;
    private String[] images;

    public Event(long eventId, long eventStart, long hall, String eventType, long ticketsAvailable, long minPrice, long maxPrice, String eventName, String artist, String description, String[] images) {
        this.eventId = eventId;
        this.eventStart = eventStart;
        this.hall = hall;
        this.eventType = eventType;
        this.ticketsAvailable = ticketsAvailable;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.eventName = eventName;
        this.artist = artist;
        this.description = description;
        this.images = images;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public long getEventStart() {
        return eventStart;
    }

    public void setEventStart(long eventStart) {
        this.eventStart = eventStart;
    }

    public long getHall() {
        return hall;
    }

    public void setHall(long hall) {
        this.hall = hall;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String  eventType) {
        this.eventType = eventType;
    }

    public long getTicketsAvailable() {
        return ticketsAvailable;
    }

    public void setTicketsAvailable(long ticketsAvailable) {
        this.ticketsAvailable = ticketsAvailable;
    }

    public long getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(long minPrice) {
        this.minPrice = minPrice;
    }

    public long getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(long maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }
}
