package com.example.biletiki2;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TextView;

public class RegistrationInput extends AppCompatActivity implements View.OnClickListener {
    private Spinner genderSpinner;
    private ConstraintLayout regInpConstr;
    private TextView topRegInpTxt, headerTxt, reg1Txt, reg2Txt, reg3Txt, reg4Txt, reg5Txt, reg6Txt, reg7Txt, reg8Txt;
    private Button regBtn;
    private ImageView backImg;
    private TextInputEditText nameInp, surnameInp, emailInp, passwordInp, phoneInp, cityInp, countryInp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_input);
        genderSpinner = findViewById(R.id.gender_sp);
        regInpConstr = findViewById(R.id.reg_inp_constr);
        topRegInpTxt = findViewById(R.id.top_reg_inp_txt);
        headerTxt = findViewById(R.id.header_txt);
        reg1Txt = findViewById(R.id.reg1_txt);
        reg2Txt = findViewById(R.id.reg2_txt);
        reg3Txt = findViewById(R.id.reg3_txt);
        reg4Txt = findViewById(R.id.reg4_txt);
        reg5Txt = findViewById(R.id.reg5_txt);
        reg6Txt = findViewById(R.id.reg6_txt);
        reg7Txt = findViewById(R.id.reg7_txt);
        reg8Txt = findViewById(R.id.reg8_txt);
        regBtn = findViewById(R.id.register_btn);
        backImg = findViewById(R.id.back_img);
        nameInp = findViewById(R.id.name_inp);
        surnameInp = findViewById(R.id.surname_inp);
        emailInp = findViewById(R.id.email_inp);
        passwordInp = findViewById(R.id.password_inp);
        phoneInp = findViewById(R.id.phone_inp);
        cityInp = findViewById(R.id.city_inp);
        countryInp = findViewById(R.id.country_inp);

        backImg.setOnClickListener(this);
        regBtn.setOnClickListener(this);

        setAdapter();
    }

    private void setAdapter() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.mr, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_img) {
            finish();
        } else if (v.getId() == R.id.register_btn) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
